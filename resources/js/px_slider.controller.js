(function() {
	'use-strict';
	/*	Controller to manage the slider widget
	link: https://predixdev.github.io/predix-ui/?show=px-slider&type=component	*/
	
	function PxSliderStyledCtrl($scope, $timeout, $element) {
		var ctrl = this,
			slider = $('px-slider', $element),
			ctx = $scope.DECISYON.target.content.ctx,
			refObjId = ctx.instanceReference.value,
			target = $scope.DECISYON.target,
			mshPage = target.page,
			baseName = ctx.baseName.value,
			enableRange	= ctx.$pxSliderEnableRange.value,
			unbindWatches = [],
			startParamToExport = baseName + '_Start',
			endParamToExport = baseName + '_End',
			DEFAULT_MAX = '100',
			DEFAULT_MIN = '1',
			DEFAULT_STEP = '1',
			sendParamOnMove,
			EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND',
			pxSlider;
			
		/*  It adjust size ad fix overflow */
		var	getAdjustedSizing = function(size) {
			var	adjustedSize	=	size.replace(/%/g, '').trim();
			return (parseInt(adjustedSize)) ? parseInt(adjustedSize) : '';
		};
		/*  Destroy the widget reference */
		var destroyWidget = function() {
			var i;
			for (i = 0; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};

		/* updates the widget when mock  */
		var updateSilder = function(ctx) {
			ctrl.manageParamsFromContext(ctx);
			setDisableAttributeValue();
			applyDimensionsToWebComponent(ctx);
		};

		/*  Watch on widget context to observe parameter changing after loading  */
		var attachListeners = function() {
			$scope.$watch('DECISYON.target.content.ctx', function(newCtx, oldCtx) {
				if (!angular.equals(newCtx, oldCtx)){
					updateSilder(newCtx);
				}
			}, true);
			//Listener on destroy angular element; in this case we destroy all watch and listener
			$scope.$on('$destroy', destroyWidget);
		};

		/* 	Set the container widget size.If you set both this and height to "auto", the chart will expand to fill its containing element.
		Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value. */
		var applyDimensionsToWebComponent = function(ctx) {
			ctrl.width	= (getAdjustedSizing(ctx['min-width'].value));
			ctrl.height	= (getAdjustedSizing(ctx['min-height'].value));
		};

		/*  Checks the disable property's value and returns true/false for respective conditions */
		var setBooleanDisable = function(context) {
			if (context === 1 || context === true) {
				return 'true';
			} else {
				return 'false';
			}
		};

		/* sets the disable property's value to the widget */
		var setDisableAttributeValue = function() {
			if (ctrl.disable === 'true') {
				pxSlider.setAttribute('disabled', 'true');
			} else {
				pxSlider.removeAttribute('disabled', 'false');
			}
		};

		/*  function to refresh the slider handler in set interval so that it would not get blocked */
		var toRefreshtheslider = function() {
			$timeout(function() {
				pxSlider._onIronResize();
			},1800);
		};
		/*  Managind the param values when changed  */
		ctrl.manageParamsFromContext = function(newContext) {
			var startParamToImport = (enableRange) ? '_PARAM_' + startParamToExport : '_PARAM_' + baseName,
				endParamToImport   = '_PARAM_' + endParamToExport,
				startValueFromParams = (angular.isDefined(newContext[startParamToImport]) && !angular.equals(newContext[startParamToImport].value, EMPTY_PARAM_VALUE)) ? newContext[startParamToImport].value : '',
				endValueFromParams   = (angular.isDefined(newContext[endParamToImport]) && !angular.equals(newContext[endParamToImport].value, EMPTY_PARAM_VALUE)) ? newContext[endParamToImport].value : '';
			ctrl.startValue = startValueFromParams;
			ctrl.value    = startValueFromParams;
			ctrl.endValue = endValueFromParams;
			ctrl.disable  = setBooleanDisable(newContext.$pxSliderDisable.value);

			if (slider.length > 0 && enableRange) {
				slider.attr('end-value', ctrl.endValue);
			}
		};

		/* Event for the capturing the data,when the slider handler position is changed */
		ctrl.eventHandler = function() {
								
								var startInput = document.querySelector('#' + ctrl.widgetID + ' input[id=inputStart]'),
									paramNameToSend = (enableRange) ? startParamToExport : baseName;
								
								/* Event fired when textbox value for start Input is changed */
								startInput.addEventListener('change', function(e) {
									ctrl.startVal = e.target.value;
									if (mshPage) {
										target.sendParamChanged(paramNameToSend, ctrl.startVal);
									}
								});
								/* Captures values from the slider when the user changes the handler position */
								var sliderMoveComplete = pxSlider._debouncers.slider_move.boundComplete;
								
								pxSlider._debouncers.slider_move.boundComplete = function() {
									sliderMoveComplete();
									if (mshPage) {
										sendParamOnMove = new Date().getTime();
										$timeout(function() {
											var now = new Date().getTime();
											if (now >= (sendParamOnMove + 500)) {
												target.sendParamChanged(paramNameToSend, startInput.value);
											}
										}, 500);
									}
								};
								/* if enable range value is true, */
								if (enableRange) {
									var endInput = document.querySelector('#' + ctrl.widgetID + ' input[id=inputEnd]');
									endInput.addEventListener('change', function(e) {
										ctrl.endVal = e.target.value;
										if (mshPage) {
											target.sendParamChanged(endParamToExport, ctrl.endVal);
										}
									});
									
									/* Captures values from the slider when the user changes the handler position */
									var sliderMoveEndComplete = pxSlider._debouncers.slider_move_end.boundComplete;
									pxSlider._debouncers.slider_move_end.boundComplete = function() {
										sliderMoveEndComplete();
										if (mshPage) {
											sendParamOnMove = new Date().getTime();
											$timeout(function() {
												var now = new Date().getTime();
												if (now >= (sendParamOnMove + 500)) {
													target.sendParamChanged(endParamToExport, endInput.value);
												}
											}, 500);
										}
									};
								}
							};

		/*  When the predix ui widget was completed. Callback called when dcyOnLoad directive has completed.  */
		ctrl.setPredixSliderLibraryLoaded = function() {
			if (slider.length > 0) {
				pxSlider = document.querySelector('#' + ctrl.widgetID);
				pxSlider._readySelf(); // this is to refersh the widget after the error state due to invalid value in disable page param.
				setDisableAttributeValue();
				applyDimensionsToWebComponent(ctx);
				ctrl.eventHandler();
				toRefreshtheslider();
			}
		};
        
		/*  Initialize  */
		ctrl.inizialize = function() {
			ctrl.widgetID = 'pxSlider_' + refObjId;
			ctrl.widgetContainerID = 'pxSliderContainer_' + refObjId;
			ctrl.enableRange = enableRange;
			ctrl.max = (ctx.$pxSliderMax.value !== '') ?  ctx.$pxSliderMax.value : DEFAULT_MAX;
			ctrl.min = (ctx.$pxSliderMin.value !== '') ? ctx.$pxSliderMin.value : DEFAULT_MIN;
			ctrl.step = (ctx.$pxSliderStep.value !== '') ? ctx.$pxSliderStep.value : DEFAULT_STEP;
			ctrl.manageParamsFromContext(ctx);
			attachListeners(ctx);
		};
		ctrl.inizialize();
	}

	PxSliderStyledCtrl.$inject = ['$scope', '$timeout', '$element'];

	DECISYON.ng.register.controller('pxSliderStyledPredixCtrl', PxSliderStyledCtrl);

}());